import React from "react";
import {render} from "react-dom";
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-social/bootstrap-social.css'
import App from "./components/App";

render(<App/>, document.getElementById('root'));