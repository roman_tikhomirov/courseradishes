import React from "react";
import {Link} from "react-router-dom";
import {Loading} from './LoadingComponent'
import {baseUrl} from '../assets/baseUrl'

    function RenderMenuItem({dish}){
        return(
            <div className="card">
                <Link to={`/menu/${dish.id}`}>
                    <img
                        width='100%'
                        src={baseUrl + dish.image}
                        alt={dish.name}
                        className="rounded-circle"
                    />
                    <div className="card-img-overlay">
                        <h4 className="card-title">
                            {dish.name}
                        </h4>
                    </div>
                </Link>
            </div>
        );
    }

    const Menu = (props) => {
        const menu = props.dishes.dishes.map((dish) => {
            return(
                <div key={dish.id} className='col-sm-12 col-lg-3'>
                    <RenderMenuItem dish={dish}/>
                </div>
            )
        });

        if (props.dishes.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.dishes.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h4>{props.dishes.errMess}</h4>
                        </div>
                    </div>
                </div>
            );
        }
        else
            return (
                <div className="container-fluid">
                    <div className='row'>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to='/home'>Home</Link></li>
                            <li className='breadcrumb-item active'>Menu</li>
                        </ol>
                        <div className="col-12">
                            <h3>Menu</h3>
                            <hr />
                        </div>
                    </div>
                    <div className='row menu'>
                        {menu}
                    </div>
                </div>
            )
    }

export default Menu