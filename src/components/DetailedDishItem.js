import React from "react";
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {Link} from "react-router-dom";
import CommentForm from "./CommentForm";
import {Loading} from './LoadingComponent'
import {baseUrl} from '../assets/baseUrl'
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

function RenderDish({dish}) {
        return(
            <div className='col-lg-6 col-md-12 mx-auto'>
                <FadeTransform
                    in
                    transformProps={{
                        exitTransform: 'scale(0.5) translateY(-50%)'
                    }}>
                    <div className="card">
                        <img
                            width='100%'
                            src={baseUrl + dish.image}
                            alt={dish.name}
                            className="card-img-top"
                        />
                        <div className="card-body">
                            <h4 className="card-title">
                                {dish.name}
                            </h4>
                            <h6 className='card-text'>
                                {dish.description}
                            </h6>
                        </div>
                    </div>
                </FadeTransform>
            </div>
        );
    }

    function RenderComments({comments, postComment, dishId}) {
        const list = comments.map((someComment) => parseList(someComment))
        return (
            <div className="col-lg-6 col-md-12 mx-auto heigh='fit-content'">
                <div className='container'>
                    <h4>Comments</h4>
                    <ul>
                        <Stagger in>
                            {list}
                        </Stagger>
                    </ul>
                    <CommentForm dishId={dishId} postComment={postComment} />
                </div>
            </div>
        );
    }

    function parseList(someComment) {
        return (
            <Fade in>
                <li key={someComment.id}>
                    <div>{someComment.comment}</div>
                    <div>
                        <p>{'--- ' + someComment.author + ', ' + new Intl.DateTimeFormat('en-US', {year : 'numeric', month : 'short', day : '2-digit'}).format(new Date(Date.parse(someComment.date)))}</p>
                    </div>
                </li>
            </Fade>
        )
    }

    const DetailedDish = (props) => {
        const detailedItem = props.selectedDish ? <RenderDish dish={props.selectedDish}/> : null;
        const detailedComment = props.selectedDish && props.comments ? <RenderComments comments={props.comments} postComment={props.postComment} dishId={props.selectedDish.id}/> : null;

        if (props.isLoading) {
            return (
                <div className="container-fluid">
                    <div className="row">
                        <Loading/>
                    </div>
                </div>
            );
        } else if (props.errMess) {
            return (
                <div className="container-fluid">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        } else if (props.selectedDish != null) {
            return (
                <div className="container-fluid">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.selectedDish.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.selectedDish.name}</h3>
                            <hr/>
                        </div>
                    </div>
                    <div className='row'>
                        {detailedItem}
                        {detailedComment}
                    </div>
                </div>
            )
        }
    }

export default DetailedDish